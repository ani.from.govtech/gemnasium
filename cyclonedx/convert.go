package cyclonedx

import (
	"encoding/json"
	"fmt"
	"os"
	"path"
	"sort"
	"time"

	"github.com/google/uuid"

	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/finder"
	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/scanner"
	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/scanner/parser"
)

// ToolInfo contains information about the SBOM generator tool which is inserted into the SBOM
type ToolInfo struct {
	Name    string
	Vendor  string
	Version string
}

const (
	propertyNameInputFile      = "gitlab:dependency_scanning:input_file"
	propertyNamePackageManager = "gitlab:dependency_scanning:package_manager"

	// specVersion is the CycloneDX report schema version that we implement
	specVersion = "1.4"

	// bomFormat specifies the format of the BOM. This helps to identify the file as CycloneDX since BOMs
	// do not have a filename convention nor does JSON schema support namespaces. This value MUST be "CycloneDX".
	bomFormat = "CycloneDX"

	// email used in the CycloneDX report
	email = "support@gitlab.com"
)

// OutputSBOMs outputs each CycloneDX report to the path where the lock/build file was detected
func OutputSBOMs(targetDir string, sboms []SBOM) error {
	for _, sbom := range sboms {
		absoluteSBOMPath := path.Join(targetDir, sbom.OutputFilePath)
		outFile, err := os.Create(absoluteSBOMPath)
		if err != nil {
			return err
		}
		defer outFile.Close()

		enc := json.NewEncoder(outFile)
		enc.SetIndent("", "  ")

		if err := enc.Encode(sbom); err != nil {
			return err
		}
	}

	return nil
}

// ToSBOMs converts dependency files to CycloneDX SBOMs, with components in sorted order
func ToSBOMs(scannerFiles []scanner.File, timeStamp *time.Time, toolInfo ToolInfo) []SBOM {
	sboms := []SBOM{}

	for _, scannerFile := range scannerFiles {
		sbom := SBOM{
			BomFormat:    bomFormat,
			SpecVersion:  specVersion,
			SerialNumber: uniqueSerialNumber(),
			Version:      1,
			Metadata: Metadata{
				Timestamp:  formattedTime(timeStamp),
				Properties: metadataProperties(scannerFile),
				Tools: []Tool{{
					Vendor:  toolInfo.Vendor,
					Name:    toolInfo.Name,
					Version: toolInfo.Version,
				}},
				Authors: []Author{{
					Name:  toolInfo.Vendor,
					Email: email,
				}},
			},
			PackageManager: scannerFile.PackageManager,
			PackageType:    scannerFile.PackageType,
			InputFilePath:  scannerFile.Path,
		}
		sbom.setOutputFilePath()

		for _, dependency := range scannerFile.Packages {
			purl := purl(scannerFile.PackageManager, dependency)
			component := Component{
				Name:          dependency.Name,
				Version:       dependency.Version,
				PURL:          purl,
				ComponentType: "library",
				BomRef:        purl,
			}
			sbom.Components = append(sbom.Components, component)
		}

		sbom.sortComponents()
		sboms = append(sboms, sbom)
	}

	return sboms
}

// purl or "package URL" is a URL string used to identify and locate a software package
// in a mostly universal and uniform way across programming languages, package managers,
// packaging conventions, tools, APIs and databases.
//
// Example: pkg:golang/github.com/pmezard/go-difflib@1.0.0
//
// See https://github.com/package-url/purl-spec/blob/master/PURL-SPECIFICATION.rst for specification
func purl(packageManager string, dependency parser.Package) string {
	purlType := finder.PURLTypeForPackageManager(string(packageManager))
	return fmt.Sprintf("pkg:%s/%s@%s", purlType, dependency.Name, dependency.Version)
}

func metadataProperties(scannerFile scanner.File) []MetadataProperty {
	return []MetadataProperty{
		{
			Name:  propertyNameInputFile,
			Value: scannerFile.Path,
		},
		{
			Name:  propertyNamePackageManager,
			Value: scannerFile.PackageManager,
		},
	}
}

// Every BOM generated should have a unique serial number, even if the contents of the BOM have not changed over time.
func uniqueSerialNumber() string {
	// TODO: catch err
	reportUUID, _ := uuid.NewRandom()

	return fmt.Sprintf("urn:uuid:%s", reportUUID)
}

func formattedTime(timeStamp *time.Time) string {
	t := time.Time(*timeStamp)
	layoutISO := "2006-01-02T15:04:05Z"

	return t.Format(layoutISO)
}

// setOutputFilePath sets the relative path to the SBOM file
func (sbom *SBOM) setOutputFilePath() {
	fileName := fmt.Sprintf("gl-sbom-%s-%s.cdx.json", sbom.PackageType, sbom.PackageManager)
	sbom.OutputFilePath = path.Join(path.Dir(sbom.InputFilePath), fileName)
}

// sortComponents sorts components by name, then version
func (sbom *SBOM) sortComponents() {
	sort.Slice(sbom.Components, func(i, j int) bool {
		ni, nj := sbom.Components[i].Name, sbom.Components[j].Name
		if ni == nj {
			return sbom.Components[i].Version < sbom.Components[j].Version
		}
		return ni < nj
	})
}
