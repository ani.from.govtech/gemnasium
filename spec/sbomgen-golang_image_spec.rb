require "tmpdir"
require "English"

require 'gitlab_secure/integration_test/docker_runner'
require 'gitlab_secure/integration_test/shared_examples/scan_shared_examples'
require 'gitlab_secure/integration_test/shared_examples/cyclonedx_shared_examples'
require 'gitlab_secure/integration_test/spec_helper'

describe "running image" do
  let(:fixtures_dir) { "qa/fixtures" }
  let(:expectations_dir) { "qa/expect" }

  def image_name
    ENV.fetch("TMP_IMAGE", "gemnasium:latest")
  end

  context "with no project" do
    before(:context) do
      @output = `docker run -t --rm -w /app #{image_name}`
      @exit_code = $CHILD_STATUS.to_i
    end

    it "shows there is no match" do
      expect(@output).to match(/no match in \/app/i)
    end

    describe "exit code" do
      specify { expect(@exit_code).to be 0 }
    end
  end

  # rubocop:disable RSpec/MultipleMemoizedHelpers
  context "with test project" do
    let(:global_vars) { {} }
    let(:project) { "any" }
    let(:relative_expectation_dir) { project }
    let(:variables) { {} }
    let(:command) { [] }
    let(:script) { nil }
    let(:offline) { false }
    let(:target_dir) { File.join(fixtures_dir, project) }

    let(:scan) do
      GitlabSecure::IntegrationTest::DockerRunner.run_with_cache(
        image_name, fixtures_dir, target_dir, @description,
        command: command,
        script: script,
        offline: offline,
        variables: global_vars.merge(variables))
    end

    context "containing multiple sub-projects with subdirs" do
      let(:project) { "multi-project/subdirs" }
      let(:relative_sbom_paths) { ["go-project/gl-sbom-go-go.cdx.json"] }

      it_behaves_like "non-empty CycloneDX files"
      it_behaves_like "recorded CycloneDX files"
      it_behaves_like "valid CycloneDX files"
    end

    context "with go-modules" do
      let(:project) { "go-modules/gomod/default" }
      let(:relative_sbom_paths) { ["gl-sbom-go-go.cdx.json"] }

      it_behaves_like "non-empty CycloneDX files"
      it_behaves_like "recorded CycloneDX files"
      it_behaves_like "valid CycloneDX files"

      it_behaves_like "expected CycloneDX metadata tool-name", "sbomgen-golang"

      context "when setting ADDITIONAL_CA_CERT_BUNDLE" do
        let(:variables) do
          { ADDITIONAL_CA_CERT_BUNDLE: "testing" }
        end

        let(:script) do
          <<~HERE
          #!/bin/sh

          sbomgen-golang r
          # Output the certificates
          tail -n 1 /etc/ssl/certs/ca-certificates.crt
          HERE
        end

        it_behaves_like "non-empty CycloneDX files"
        it_behaves_like "recorded CycloneDX files"
        it_behaves_like "valid CycloneDX files"

        it "appends the ADDITIONAL_CA_CERT_BUNDLE variable content to /etc/ssl/certs/ca-certificates.crt" do
          expect(scan.combined_output).to match(variables[:ADDITIONAL_CA_CERT_BUNDLE])
        end
      end

      context "when offline" do
        let(:offline) { true }

        context "when gemnasium-db update disabled" do
          let(:variables) do
            { GEMNASIUM_DB_UPDATE_DISABLED: "true" }
          end

          it "selects the go.sum parser" do
            expect(scan.combined_output).to match(/Selecting "go.sum" parser for "[^"]+".*False positives may occur/)
          end
        end
      end

      # TODO: Should we output an SBOM with no components here? We don't currently output an SBOM if there are no components.
      # context "when excluding go.sum with DS_EXCLUDED_PATHS" do
      #   let(:variables) { { "DS_EXCLUDED_PATHS": "/go.sum" } }

      #   describe "created report" do
      #     it_behaves_like "empty report"
      #     it_behaves_like "valid report"
      #   end
      # end

      context "when in a subdirectory" do
        let(:project) { "go-modules/gomod/subdir" }
        let(:relative_sbom_paths) { ["subdir/gl-sbom-go-go.cdx.json"] }

        it_behaves_like "non-empty CycloneDX files"
        it_behaves_like "recorded CycloneDX files"
        it_behaves_like "valid CycloneDX files"
      end
    end
  end
end
