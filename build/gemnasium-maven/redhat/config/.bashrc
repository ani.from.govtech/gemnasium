#!/bin/bash -l

# switch_to_java switches to a given version of Java.
#
# It defaults to Java 17 when the requested version
# is not supported, or when it's not set.
function switch_to_java() {
  local java_version=$1
  local default_java_version=17
  local openjdk_pkg

  case $java_version in
    11 | 17)
      openjdk_pkg="java-${java_version}-openjdk"
      ;;
    8)
      openjdk_pkg="java-1.8.0-openjdk"
      # For OpenJDK 1.8.0, JAVA_HOME corresponds to "jre" directory
      ;;
    *)
      # switch to Java 17 by default when version is not supported or it hasn't been provided
      echo "Unable to find Java version '${java_version}', defaulting to version '${default_java_version}'"
      openjdk_pkg="java-17-openjdk"
      ;;
  esac

  switch_to_openjdk_pkg "${openjdk_pkg}"
  echo "Using java version '${openjdk_pkg}'"
}

# switch_to_openjdk_pkg sets the "java" command and JAVA_HOME
# to use the specified openjdk package. It shows no messages.
function switch_to_openjdk_pkg() {
  local openjdk_pkg=$1
  local java_cli_path
  local java_cli_dir

  # get path of java CLI that matches the openjdk package
  # https://access.redhat.com/documentation/en-us/openjdk/11/html-single/configuring_openjdk_11_on_rhel/index
  java_cli_path=$(alternatives --display java | grep "family ${openjdk_pkg}" | cut -d' ' -f1)

  # set path to java binary
  alternatives --set java "$java_cli_path"

  # set JAVA_HOME as the parent directory of the directory where the "java" CLI is
  java_cli_dir=$(dirname "$java_cli_path")
  JAVA_HOME=$(dirname "$java_cli_dir")
  export JAVA_HOME
}

# switch_to switches to a given tool and version
# Example: switch_to 'java' '11'
#
# The above command will switch to the adopt-openjdk-11.0.7+10 version of java
function switch_to() {
  local tool=$1
  local given_version=$2
  local tool_versions
  local selected_version

  # fetch line matching tool, ignore the first word and use 'read' to turn space-delimited words into an array
  IFS=" " read -r -a tool_versions <<<"$(grep "$tool" "$HOME/.tool-versions" | cut -d ' ' -f2-)"

  # if no match is found, default to first entry in .tool-versions file
  default_version=${tool_versions[0]}

  for tool_version in "${tool_versions[@]}"; do
    if [[ $tool_version = $given_version* ]]; then
      selected_version=$tool_version
      break
    fi
  done

  # no match found
  if [[ -z ${selected_version:-} ]]; then
    echo "Unable to find ${tool} version '${given_version}', defaulting to version '${default_version}'"
    selected_version=$default_version
  fi

  echo "Using ${tool} version '${selected_version}'"
  asdf shell "$tool" "$selected_version"
}

function enable_dev_mode() {
  unset HISTFILESIZE
  unset HISTSIZE
  export EDITOR=vim
  export LOG_LEVEL=debug
  set -o vi
  apt-get update -y
  apt-get install -y --no-install-recommends vim less shellcheck
}

# shellcheck source=/dev/null
. "$ASDF_DATA_DIR/asdf.sh"
# shellcheck source=/dev/null
. "$ASDF_DATA_DIR/completions/asdf.bash"

# silently switch to the default version of Java to behave like asdf-java
switch_to_openjdk_pkg "java-17-openjdk"
